"use strict";

app.filter("timeDiff", function(dateFilter) {
    function timeDiff(start, end) {
        if (start !== null) {
            let start2 = new Date(start);

        let diff = start2 - end, richtung = diff > 0 ? 'in' : 'ago';

        diff = Math.abs(diff);

        if (diff < 60 * 1000) {
            var format = "s 's'";
        } else if (diff < 3600 * 1000) {
            var format = "m 'min' s 's'";
        } else if (diff < 24 * 3600 * 1000) {
            var format = "H 'h' m 'min'";
        } else if (diff < 7 * 24 * 3600 * 1000) {
            var format = "H 'h'";
            diff = diff % (24 * 3600 * 1000);
        } else {
            var format = "H 'h'";
        }

        let tage = Math.floor(diff / (24 * 3600 * 1000));
        tage = tage ? `${tage}d ` : '';

        return `${tage}${dateFilter(new Date(diff), format, 'UTC')} ${richtung}`;
        } else {
            return "never";
        }
    }
    return timeDiff;
})