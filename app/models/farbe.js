"use strict";

app.factory("Farbe", function () {
    function Farbe(title, red, green, blue, hue, saturation, value, id, updated) {
        this.title = title;
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.hue = hue;
        this.saturation = saturation;
        this.value = value;
        this.id = id;
        this.updated = updated;

        this.hex = wert => {
            return angular.isNumber(wert)
                ? ("0" + wert.toString(16)).substr(-2)
                : "??";
        }

        this.hexWert = () => {
            return ("#" + this.hex(this.red) + this.hex(this.green) + this.hex(this.blue)).toUpperCase();
        };

        this.helligkeit = () => {
            return (0.2126*this.red + 0.7152*this.green + 0.0722*this.blue);
        }
    }
    return Farbe;
})