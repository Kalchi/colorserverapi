"use strict";

app.component("localList", {
    templateUrl: "components/local-list.html",
    controller: "localListController",
    bindings: {}
});

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state({
        name: "local-list",
        url: "/local-list",
        component: "localList",
        params: {
            farbe: null
        }
    });
});


app.controller("localListController", function ($log, Farbe, $stateParams, StorageService, $timeout) {

    //$log.debug("localListController()");

    this.$onInit = function () {

        this.farbListe = [];
        this.farbe = $stateParams.farbe;

        let tempStor = StorageService.load();
        if (tempStor === null) {
            console.log("Localstorage is null!");
        } else {
            this.farbListe = tempStor;
        }

        if (this.farbe !== null) {
            this.farbListe.push(this.farbe);
            StorageService.save(this.farbListe);
        }

    }

    this.clearLocal = () => {
        StorageService.clearStorage();
    }

    (function aktualisieren() {
        $timeout(aktualisieren, 1000);
    })();

});