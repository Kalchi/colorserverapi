"use strict";

app.component("farbListe", {
    templateUrl: "components/farb-liste.html",
    controller: "FarbListeController",
    bindings: {}
});


app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state({
        name: "farb-liste",
        url: "/farb-liste",
        component: "farbListe"
    });
    $urlRouterProvider.otherwise("/farb-liste");
});


app.controller("FarbListeController", function ($log, ApiReqService, Farbe, $timeout) {

    //$log.debug("FarbListeController()");

    this.$onInit = function () {
        this.sortingMenueItems = ["Title", "Red", "Green", "Blue", "Hue", "Saturation", "Value"];
        this.farbListe = [];

        ApiReqService.getAllColors()
            .then(response => {
                let farbArray = response.data;
                for (let farbe of farbArray) {
                    this.farbListe.push(new Farbe(farbe.title, farbe.red, farbe.green, farbe.blue, farbe.hue,
                        farbe.saturation, farbe.value, farbe.id, farbe.updated));
                }
                console.log(farbArray);
        });;
    };

    this.search = () => {
        ApiReqService.searchColors(this.searchInput).then(response => {
            this.farbListe = [];
            let farbArray = response.data;
            for (let farbe of farbArray) {
                this.farbListe.push(new Farbe(farbe.title, farbe.red, farbe.green, farbe.blue, farbe.hue,
                    farbe.saturation, farbe.value, farbe.id, farbe.updated))
            }
        })
    };

    this.sortList = (index) => {
        ApiReqService.sortColors(this.sortingMenueItems[index]).then(response => {
            this.farbListe = [];
            let farbArray = response.data;
            for (let farbe of farbArray) {
                this.farbListe.push(new Farbe(farbe.title, farbe.red, farbe.green, farbe.blue, farbe.hue,
                    farbe.saturation, farbe.value, farbe.id, farbe.updated))
            }
        });
    };

    (function aktualisieren() {
        $timeout(aktualisieren, 1000);
    })();
});