"use strict";

app.component("farbItem", {
    templateUrl: "components/farb-item.html",
    controller: "FarbItemController",
    bindings: {
        farbe: "<",
        showPlus: "<"
    }
});

app.controller("FarbItemController", function ($log, Farbe) {
    //$log.debug("FarbItemController()");

    this.jetzt = () => {
        return new Date();
    };
});