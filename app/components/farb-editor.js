"use strict";

app.component("farbEditor", {
    templateUrl: "components/farb-editor.html",
    controller: "FarbEditorController",
    bindings: {}
});

app.config(function ($stateProvider) {
    $stateProvider.state({
        name: "farb-editor",
        component: "farbEditor",
        params: {
            farbe: null
        }
    });
});

app.controller("FarbEditorController", function ($log, Farbe, $stateParams, $state, $http) {
    this.$onInit = function () {
        if ($stateParams.farbe === null) {
            $stateParams.farbe = new Farbe("",0,0,0,0,0,0,0)
        }
        this.farbe = $stateParams.farbe;
        this.name = this.farbe.title;
        this.localHex = this.farbe.hexWert();
        this.red = this.farbe.red;
        this.green = this.farbe.green;
        this.blue = this.farbe.blue;
        this.id = this.farbe.id;
        this.updated = this.farbe.updated;
    }

    this.updateColor = () => {
        $http.put("http://localhost:8081/api/colours/" + this.farbe.id, {
            "title": this.name,
            "red": this.red,
            "green": this.green,
            "blue": this.blue
        })
            .then(response => {
                $state.go('farb-liste')
            })
            .catch(e => {
                this.error = "Error: " + e;
            })
    };

    this.saveColor = () => {
        $http.post("http://localhost:8081/api/colours/", {
            "title": this.name,
            "red": this.red,
            "green": this.green,
            "blue": this.blue
        })
            .then (response => {
                this.error = undefined;
                this.success = "Farbe erfolgreich hinzugefügt!";
            })
            .catch(e => {
                this.success = undefined;
                this.error = "Error: " + e;
            })
    }

    this.colorChange = () => {
        this.localHex = this.currentHex();
    };

    this.currentHex = () => {
        return ("#" + this.farbe.hex(this.red) + this.farbe.hex(this.green) + this.farbe.hex(this.blue)).toUpperCase();
    };

    this.helligkeit = () => {
        return (0.2126*this.red + 0.7152*this.green + 0.0722*this.blue);
    }
});