"use strict";

app.service("StorageService", function ($log, Farbe) {

    //$log.debug("StorageService()");

    const FARBE_KEY = 'farbe';

    this.load = () => {
        let tempStor = JSON.parse(localStorage.getItem(FARBE_KEY));
        let tempArr = [];
        if (tempStor !== null) {
            for (let i = 0; i < tempStor.length; i++) {
                tempArr.push(new Farbe(tempStor[i].title, tempStor[i].red, tempStor[i].green, tempStor[i].blue,
                    tempStor[i].hue, tempStor[i].saturation, tempStor[i].value, tempStor[i].id, tempStor[i].updated));
            }
        }
        return tempArr;
    };

    this.save = (farben) => {
        localStorage.setItem(FARBE_KEY, JSON.stringify(farben));
    };

    this.clearStorage = () => {
        localStorage.removeItem(FARBE_KEY);
    };

});