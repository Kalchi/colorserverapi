"use strict";

app.service("ApiReqService", function ($log, $http) {

    //$log.debug("ApiReqService");

    this.getAllColors = () => {
        return $http.get("http://localhost:8081/colours");
    };

    this.searchColors = (title) => {
        return $http.get("http://localhost:8081/colours?title=" + title);
    }

    this.sortColors = (sort) => {
        return $http.get("http://localhost:8081/colours?sort=" + sort);
    }
});